package services.beans;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class Recipes {
    Map<Integer, Integer> count;
    Map<Integer, List<Integer>> ingredients;

    public Recipes(){
        count = new HashMap<Integer, Integer>();
        ingredients = new HashMap<Integer, List<Integer>>();
    }

    public void addIngredients(int recipeId, List<Integer> ingredients){
        count.put(recipeId, 0);
        this.ingredients.put(recipeId, ingredients);
    }

    public List<Integer> getIngredients(int recipeId){
        return ingredients.get(recipeId);
    }

    public Set<Integer> getRecipesIds(){
        return ingredients.keySet();
    }

    public void setCount(int recipeId, int count){
        this.count.put(recipeId, count);
    }

    public Map<Integer, Integer> getCount(){
        return count;
    }
}
