package services.beans;

import java.util.HashMap;
import java.util.Map;

public class Products {
    Map<Integer, Integer> count;

    public Products(){
        count = new HashMap<Integer, Integer>();
    }

    public void addProduct(int productId, int quantity){
        if(!count.containsKey(productId)){
            count.put(productId, quantity);
            return;
        }
        count.put(productId, count.get(productId) + quantity);
    }

    public int productCount(int productId){
        if(!count.containsKey(productId))
            return 0;
        return count.get(productId);
    }

}
