package services.main;

import services.DBOperations;
import services.beans.Products;
import services.beans.Recipes;

import java.util.Set;
import java.util.TreeSet;

public class Main {

    public static void main(String args[]){

        while(true) {

            System.out.println("Start matching");
            matchRecepies();
            try {
                Thread.sleep(1000);
            } catch (Exception e){
                e.printStackTrace();
            }

        }

    }

    public static void matchRecepies(){
        Recipes recipes = DBOperations.getRecipes();
        Products products = DBOperations.getProductsInArea("1");

        for(int recipeId: recipes.getRecipesIds()){
            int minValue = 1000000;

            for(int ingredientId: recipes.getIngredients(recipeId)){
                minValue = Math.min(minValue, products.productCount(ingredientId));
            }

            recipes.setCount(recipeId, minValue);
        }

        DBOperations.deletePossibleRecipes();
        DBOperations.addPossibleRecipes(recipes.getRecipesIds(), recipes.getCount());

        Set<Integer> set = new TreeSet<Integer>();
        for(int id: recipes.getRecipesIds())
            if(recipes.getCount().get(id) != 0)
                set.add(id);

        DBOperations.changeStatus(set);

    }


}
