package services;

import com.google.cloud.datastore.*;
import services.beans.Products;
import services.beans.Recipes;

import java.text.SimpleDateFormat;
import java.util.*;

public class DBOperations {

    private static final Datastore datastore = DatastoreOptions.getDefaultInstance().getService();

    private static final KeyFactory possibleRecipes = datastore.newKeyFactory().setKind("PossibleRecipes");
    private static final KeyFactory recipesIntents = datastore.newKeyFactory().setKind("RecipesIntents");

    public static Products getProductsInArea(String areaId){

        Query<Entity> query = Query.newEntityQueryBuilder()
                .setKind("UserProducts")
                .build();
        QueryResults<Entity> userProducts = datastore.run(query);

        Products prod = new Products();

        while (userProducts.hasNext()) {
            Entity product = userProducts.next();
            prod.addProduct(getInt(product,"productId"), getInt(product, "quantity"));
        }

        return prod;
    }

    public static Recipes getRecipes(){
        Query<Entity> query = Query.newEntityQueryBuilder()
                .setKind("Recipes")
                .build();
        QueryResults<Entity> recipes = datastore.run(query);

        Recipes recp = new Recipes();


        while (recipes.hasNext()) {

            Entity recipe = recipes.next();
            recp.addIngredients(getInt(recipe,"RecipeId"), getList(recipe.getString("Ingredients")));
        }

        return recp;
    }

    public static void deletePossibleRecipes(){
        Query<Entity> query = Query.newEntityQueryBuilder()
                .setKind("PossibleRecipes")
                .build();
        QueryResults<Entity> recipes = datastore.run(query);

        while (recipes.hasNext()) {
            Entity recipe = recipes.next();
            try {
                datastore.delete(possibleRecipes.newKey(recipe.getKey().getId()));
            } catch(Exception e){
                e.printStackTrace();
            }
        }

    }

    public static void addPossibleRecipes(Set<Integer> ids, Map<Integer, Integer> count){
        for(int id: ids){
            if(count.get(id) == 0)
                continue;

            Key key = datastore.allocateId(possibleRecipes.newKey());
            Entity entity = Entity.newBuilder(key)
                    .set("recipeId", id)
                    .set("count", count.get(id))
                    .build();
            datastore.put(entity);
        }
    }

    public static void changeStatus(Set<Integer> ids){

        Query<Entity> query = Query.newEntityQueryBuilder()
                .setKind("RecipesIntents")
                .build();
        QueryResults<Entity> recipes = datastore.run(query);

        while (recipes.hasNext()) {
            Entity recipe = recipes.next();

            Entity task = Entity.newBuilder(datastore.get(recipe.getKey())).set("active", "1").build();
            datastore.update(task);
        }

        deactivateAllOthers(ids);
    }

    private static void deactivateAllOthers(Set<Integer> ids){
        System.out.println("Deactivate ids: " + ids);

        Query<Entity> query = Query.newEntityQueryBuilder()
                .setKind("RecipesIntents")
                .build();
        QueryResults<Entity> recipes = datastore.run(query);

        while (recipes.hasNext()) {
            Entity recipe = recipes.next();

            if(!ids.contains(Integer.parseInt(recipe.getString("recipeId")))){
                List<Integer> ingredients = getRecipeIngredients(recipe.getString("recipeId"));
                Entity task = Entity.newBuilder(datastore.get(recipe.getKey()))
                        .set("active", "0")
                        .set("queues", emptyQueues(ingredients))
                        .set("users", "")
                        .build();
                datastore.update(task);
            }

        }


    }

    private static List<Integer> getRecipeIngredients(String recipeId){
        Query<Entity> query = Query.newEntityQueryBuilder()
                .setKind("Recipes")
                .setFilter(StructuredQuery.PropertyFilter.eq("RecipeId", recipeId))
                .build();
        QueryResults<Entity> recipes = datastore.run(query);

        if(recipes.hasNext()){
            Entity entity = recipes.next();

            String elems[] = entity.getString("Ingredients").split(",");
            List<Integer> list = new ArrayList<Integer>();

            for(String elem: elems) {
                list.add(Integer.parseInt(elem));
            }

            return list;
        }

        return null;
    }

    private static int getInt(Entity ent, String name){
        return Integer.parseInt(ent.getString(name));
    }

    private static List<Integer> getList(String unparsedList){
        System.out.println(unparsedList);

        String elems[] = unparsedList.split(",");
        List<Integer> list = new ArrayList<Integer>();

        for(String elem: elems){
            list.add(Integer.parseInt(elem));
        }

        return list;
    }

    private static String emptyQueues(List<Integer> ingredients){
        String ret = "";

        for(int i: ingredients){
            ret += i + "-;";
        }

        return ret.substring(0, ret.length()-1);
    }



}
